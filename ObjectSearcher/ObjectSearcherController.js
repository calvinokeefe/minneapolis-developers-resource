({

    enterSearch : function(component, event, helper){
        if(event.which == 13){
            helper.findSObjects(component, event, helper);
        }
    },

    buttonSearch : function(component, event, helper){
        helper.findSObjects(component, event, helper);
    },

    navigateToSObject : function(component, event, helper) {
        var recId = event.target.value;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
    },


    hideDropdownTimer : function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                helper.hideDropdown(component, event, helper);
            }), 200
        );
    },


})
