({
    //shows user new chat notificaiton which can accept and navigate to the chat
    onWorkAssigned: function(component, event, helper) {
        var workItemId = event.getParam('workItemId');
        var workId = event.getParam('workId');
        //if users has notifications blocked it will ask them if they want to allow these notifications
        if (Notification.permission !== "granted"){
            console.log('permission to display notification denied');
            Notification.requestPermission();
        }

        if(workItemId.startsWith('570') && Notification.permission === 'granted'){
            //show the notification to the user
            var notification = new Notification('Omni-Channel Notification', {
                body: $A.get('$Label.c.Live_Agent_Notification'),
                icon: $A.get('$Resource.chaticon')
            });
            //save the notification as an attribute so it can be closed later in the closeNotification function
            component.set('v.notification', notification);

            //if the user clicks on the notification, accepts the chat and routes them to the chat window
            notification.onclick = function () {
                //if the user does not have the browser open it brings it to the foreground
                window.focus();
                var omniAPI = component.find("omniToolkit");
                omniAPI.acceptAgentWork({workId: workId}).then(function(res) {
                    if(res){
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                          "recordId": workItemId,
                          "slideDevName": "detail"
                        });
                        navEvt.fire();
                        notification.close();
                    } else {
                        console.log("Accept work failed");
                    }
                }).catch(function(error) {
                    console.log(error);
                });
            };
        }
    },

    //closes the notification when workload is assigned or changed
    closeNotification : function(component){
        var notification = component.get('v.notification');
        if(notification != null) notification.close();
    }

})
