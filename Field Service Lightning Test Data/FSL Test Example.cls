//The below code is an example implementation of the test class, some of the variable arent included because they are org specific, for example orderId on line 40

Id permissionSetId = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource Permissions' LIMIT 1].Id;
Id permissionSetId2 = [SELECT Id FROM PermissionSet WHERE Label = 'FSL Resource License'].Id;
Database.insert(serviceUserTechMeasure);
Database.insert(serviceUserInstaller);

//assign permissions to tech measure and installer
System.runAs(serviceUserTechMeasure) {
    PermissionSetAssignment psa = new PermissionSetAssignment(
        PermissionSetId = permissionSetId, 
        AssigneeId = serviceUserTechMeasure.Id
    );
    PermissionSetAssignment psa2 = new PermissionSetAssignment(
        PermissionSetId = permissionSetId2, 
        AssigneeId = serviceUserTechMeasure.Id
    );
    List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>{psa,psa2};
    Database.insert(psaList);
}

System.runAs(serviceUserInstaller) {
    PermissionSetAssignment psa3 = new PermissionSetAssignment(
        PermissionSetId = permissionSetId, 
        AssigneeId = serviceUserInstaller.Id
    );
    PermissionSetAssignment psa4 = new PermissionSetAssignment(
        PermissionSetId = permissionSetId2, 
        AssigneeId = serviceUserInstaller.Id
    );
    List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>{psa3,psa4};
    Database.insert(psaList);
}

//Creating Work Types for the Work Order
Id measureTypeId = TestDataFactoryStatic.createFLSWorkType('Measure', 8.00, 'Hours').Id;
Id installTypeId = TestDataFactoryStatic.createFLSWorkType('Install', 2.00, 'Hours').Id;

//Creating FSL objects required to attach an assigned Tech Measure to a Work Order
WorkOrder workOrd = TestDataFactoryStatic.createFSLWorkOrder(measureTypeId, 'Tech Measure', orderId, testAccount, opportunity);
Database.insert(workOrd); 
ServiceResource servResource = TestDataFactoryStatic.createFSLServiceResource(serviceUserTechMeasure.Id, 'Calvin', 'test1', storeConfigId);
Database.insert(servResource);
TestDataFactoryStatic.createFSLServiceTerritory(servResource, 'test1');
ServiceAppointment servAppt = TestDataFactoryStatic.createFSLServiceAppointment(workOrd.Id);
Database.insert(servAppt);
AssignedResource assignResource = TestDataFactoryStatic.createFSLAssignedResource(servResource, servAppt);
Database.insert(assignResource);

//Creating FSL objects required to attach an assigned Tech Measure to a Work Order
WorkOrder workOrd2 = TestDataFactoryStatic.createFSLWorkOrder(installTypeId, 'Install', orderId, testAccount, opportunity);
Database.insert(workOrd2); 
ServiceResource servResource2 = TestDataFactoryStatic.createFSLServiceResource(serviceUserInstaller.Id, 'Calvin','test2', storeConfigId);
Database.insert(servResource2);
TestDataFactoryStatic.createFSLServiceTerritory(servResource2, 'test2');
ServiceAppointment servAppt2 = TestDataFactoryStatic.createFSLServiceAppointment(workOrd2.Id);
Database.insert(servAppt2);
AssignedResource assignResource2 = TestDataFactoryStatic.createFSLAssignedResource(servResource2, servAppt2);
Database.insert(assignResource2);
